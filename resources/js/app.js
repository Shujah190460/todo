require('./bootstrap');

//window.Vue = require('vue');

import Vue from 'vue'

//import App from './vue/app'
//
//
//const app = new Vue ({
//    el: '#app',
//    components: {App}
//});

Vue.component('app', require('./vue/app.vue').default);

const app = new Vue({
    el: '#app'
});