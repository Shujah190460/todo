<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::created([
            'name'=> 'Shujah Anwar',
            'email'=> 'shujah@fake.com',
            'password'=> Hash::make('password'),
            'remember_token'=> str_random(10),
        ]);
    }
}
